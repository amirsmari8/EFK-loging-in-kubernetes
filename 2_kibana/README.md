# kibana
for visualization .
for exporting data and sharing it (pdf, png, csv ...) .
for Alerting .
=> Canvas app is a dashboard fo sales.
=> Maps app 
=> Machine learning app (feature values based on historicall values)
=> Graph app 
# How to filter using @timestamp 
see tof 1 
# Kibana query language (KQL):
it's a simplified DSL for Elasticsearch (tof 2 ) 
we can use also the "Lucene query". 
Elasticsearch aggregations are used for all Kibana visualizations .
Aggregation group Documents together: the purpose is to retrieve analytical information from them.
Categories relevant to kibana: 
* Bucket aggregations 
* Metric Aggregations 
* pipeline aggregations 

1- Bucket aggregations: 
Create buckets "group" of documents. 
example:  we can use an aggregation named "terms" to create a buckets for each unique "field" value
Note: we cannot access the actual documents within buckets, but only the documents "count", for that we can run "metric" aggregations on them 

2- METRIC aggregations: Compute metrics over buckets using document field. as an example we can use the "sum" aggregation to get order totals for each bucket (see tof) 
other metrics aggregations: avg, min, max, ...  

3- Pipeline Aggregations: very advanced 
operate on other aggregations, not buckets. as an example: use the result "sum" aggregation in calculations.

