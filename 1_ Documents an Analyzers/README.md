# workflow: 
if we have a documents looking like this: 
{
  "marke": "honda", 
  "color": "black", 
  "milage": 25444
}
 and we want to  index this documents (using dev tools in kibana), so we're going to send "put command" to elasticc search and get back the documents :
PUT /vehicles/car/123 
{
  "marke": "honda", 
  "color": "black", 
  "milage": 25444
} 
===> vehicles is the "index", car is "type", mark, color, milage is "fields" . 
- To retrieve the documents: GET /vehicles/car/123 
the response: 
{
  "_index" : "vehicles",
  "_type" : "car",
  "_id" : "123",
  "_version" : 20,
  "_seq_no" : 19,
  "_primary_term" : 1,
  "found" : true,
  "_source" : {
    "make" : "honda",
    "color" : "black",
    "milage" : 25444
  }
} 
NOTE: we can do: GET /vehicles/car/123/_source
# update a documents 
to update a document (add new field as an example) , we can do: 
POST /vehicles/car/123/_update
{
  "doc": {
    "driver": "amir"
  }
} 

we need to add "doc". 
# delete documents 
DELETE /vehicles/car/123

# Search documents 
we can search a documents using the "_search" endpoint: 
GET /vehicles/car/_search



# Index Structure 
there is a "Strucure of indexing", we can see that: 
GET /vehicles 
the structure is: 
"aliases", "mappings", "settings"  
* mapping: contain the structure of field means it's type (float, long, Integer) 
* settings: contain number of shards and number of replicas .

# Index and type: 
Note: Index support only 1 type: 
POST /vehicles/car, if we want to add another type like "moto", it's impossible, we will need another index name.  
# shards 
shards are smaller unit of storage . 
as an exammple , we have 1000 documents for index "vehicles"=> this is a logical representation , but under the hoods  the physical representation is a "shards" as an exmple we will have 2 shards : 
    * shard P0: contain documents from [1-500] 
    * shard P1: contain documents from [501-1000] 
    * R0: is a replicas of P0 
    * R1: is a replicas of P1
 => but why shards? =>  to split storage of "vihecules index" in multiple elasticsearch. see tof  
* shards can have multiple "segments" 
# Analyzers: 
it's a step under the hood (how elastic search "index" work) => map or allocate a "document into an "index" and store it into a "shards segments"
=> this process is "ANALYSIS"  
Analyzer contain 2 part: 
  * toknizer (token): as an example split sentence into words     #very important
  * filter: as an example : lowercase , synonyms ... 

Analysis is not only during indexing but also during query and search time.  
Note: analyzer is applied on a SPECIFIC field.
elastic search has a "prebuild" analyzer (standard analyzer, simple analyzer ...) and there is also "custom analyzer"


"customers" index has 1 type "online" 
PUT /customers 
{
  "mapping": {
    "online": {      # the type
      "properties": {
        "address": {
          "type": "text"  
          "analyzer": "standard"
        }, 
        "phone": {
          "type": "integer"
        }
      }
    }
  },
  "settings": {
    "number_of_shards": 2 
    "number_of_replicas": 1
  }
}


Note: we can update the "mapping" to add new field. but if want to let elastic search to be strict with "mapping" we can add a properties "dynamic" : 
  * dynamic: flase 
  * dynamic: strict 


GET customers/_mapping/online 
{
  "dynamic": "strict"
}




# Search DSL Componenets:  
DSL: Domain Specific Language (in form of JSON )
for advanced "Search" we can use 2 type of query:
* Query  Context
* Filter Context 
=> we can combine the 2 to form more complex queries.

* 1 - Query  Context 
=> return every thing 
GET /vehicles/_search
{
  "query": {
    "match_all": {}
  }
}  
--- 
=> the "marke" field should contain the value "honda"
GET /vehicles/_search
{
  "query": {
    "match": {"marke" : "honda"}
  }
} 
--- 
=> all documents that have the field driver 
GET /vehicles/_search
{ 
  "query": {
    "exists": {"field" : "driver"}
  }
}  
--- 
to search for 2 value exmaple: marke is honda and also driver is amir we must use "must" and also "bool"

GET /vehicles/_search
{ 
  "query": {
    "bool": {
      "must": [
        {"match": {"marke": "honda"}},
        {"match": {"driver": "amir"}}
      ]
    }
  }
}  
there is also "must_not", "should" (nice to have), 
--- 
search for the word "good" in the 2 field "comment", "opinion" 
it's not "must", "good" should be in one of the 2 fields
GET /courses/_search
{
  "query": {
    "multi_match": {
      "fields": ["comment", "opinion"], 
      "query": "good"
    }
  }
}

students_enrolled is a field
GET /courses/_search
{
  "query": {
    "range": {
      "students_enrolled":  {
        "gte": 20, 
        "lte": 40
      }   
    }
  }
}

* 2 - Filter  Context 
"filter" must be wrapped into "query" and "bool". 
GET /courses/_search
{
  "query": {
    "bool": {
      "filter":  {
        "match": {"name": "smari"}
      }   
    }
  }
}
# Aggregations 
Aggregation group Documents together: the purpose is to retrieve analytical information from them.
see kibana readme
as an , we want to see how many cars models in our data: 
popular_cars: we defines this in this DSL:

GET /vehicles/cars/_search 
{
  "aggs": {
    "popular_cars": {
      "terms": {
        "field": "make.keyword"
      }
    }
  }
} 

keyword: because the data type of "make" is text
--- 
GET /vehicles/cars/_search 
{
  "aggs": {
    "popular_cars": {
      "terms": {
        "field": "make.keyword"
      }, 
      "aggs": {
        "avg_price": {
          "avg": {
            "field": "price"
          }
        }
      }
    }
  }
}
avg_price: we defines this in this DSL
avg: is a field provided by elasticsearch 

--- 
aggregation work in scope of query: 
{
  "query": {
    "match": {"color": "red"}
  }, 
  "aggs": {
    "popular_cars": {
      "terms": {
        "field": "make.keyword"
      }, 
      "aggs": {
        "avg_price": {
          "avg": {
            "field": "price"
          }
        }
      }
    }
  }
} 
# buckets and metrics 
in the previous example we create a buckets "popular_cars" based on the field "make" 