# APM (Application Perfermance Management) 
it's a "practice" to monitor application insight, so we can: 
* Improve perfermance 
* Improve user experience 
* Reduce issues and errors 
- APM helps to monitor all key aspects of our peojects: Fontend, backend, and infrastructure 
- APM monitoring performed using: 
* Metrics 
* logs 
* Events 
* Traces 
- Top APM tools: Datadog, new-relic, retrace, AWS X-Ray, Azure Application Insights,  ...  

# APM (datadog) vs MOnitoring (grafana and prometheus):
*** APM (Application Performance Monitoring) Tools (e.g., Datadog) ###############
Application-Centric: APM tools primarily focus on monitoring and optimizing the performance and behavior of applications. They provide deep insights into application code, transactions, and dependencies.

Transaction Tracing: APM tools often offer transaction tracing capabilities, allowing you to trace the journey of a request or transaction through various application components and services. This helps in diagnosing performance bottlenecks.

Code-Level Insights: APM tools can offer code-level visibility, including details about slow database queries, inefficient code paths, and application errors. Developers use this information to optimize application code.

*** Monitoring Tools (e.g., Prometheus and Grafana) ###############

Infrastructure and System-Centric: Monitoring tools like Prometheus and Grafana are more infrastructure-focused. They are designed to monitor servers, networks, and various system-level metrics.

Metric Collection: These tools excel at collecting and visualizing metrics related to system health, resource utilization (CPU, memory, disk), and network performance. They are often used for monitoring the health of servers and containers.

Alerting and Visualization: Prometheus and Grafana are known for their robust alerting and visualization capabilities. You can create custom dashboards to display system and application metrics and set up alerts based on predefined thresholds.

>>> APM tools like Datadog are specialized for in-depth application performance monitoring, code-level diagnostics, and providing insights into how application performance affects the user experience and business outcomes. On the other hand, monitoring tools like Prometheus and Grafana are versatile tools for monitoring system-level metrics, infrastructure health, and resource utilization.

# APM (datadog) vs Service Mesh (ISTIO): 
*** Service Mesh (ISTIO) ###############
Traffic Management: Service mesh solutions like Istio focus on managing traffic within microservices architectures. They provide features like load balancing, traffic routing, and fault tolerance to ensure reliable communication between services.
Security: Service meshes enhance security by providing features like authentication, authorization, and encryption for service-to-service communication. They help enforce security policies and access control.
Observability: While not as detailed as APM tools, service meshes often offer observability features such as distributed tracing and metrics collection to monitor the behavior of microservices within the mesh.

>>> Datadog primarily focuses on in-depth application performance monitoring and optimization, offering code-level insights and user experience monitoring. On the other hand, service meshes like Istio concentrate on managing communication between microservices, enhancing security, and improving the reliability of service interactions within a distributed system. While there is some overlap in observability features, these tools are often used together in modern cloud-native applications to provide end-to-end visibility and control, from application performance to service-to-service communication. 


# The choice between Datadog and Jeager 
the choice between Datadog and Jaeger depends on your specific observability requirements:

If you need a comprehensive observability platform that includes APM, infrastructure monitoring, log management, and more, Datadog is a strong choice.

If your primary focus is on distributed tracing and you prefer an open-source solution with a strong community, Jaeger is a suitable option.

In some cases, organizations use both Datadog and Jaeger together to benefit from Datadog's broader observability features while leveraging Jaeger's distributed tracing capabilities for specific troubleshooting scenarios.



# Datadog 
how does datadog collect data: 
1 from datadog agent 
2 using Datadog api 
3 integration 

############################################################################################################################################# 

# Metrics type: 
we have 4 metrics type inside datadog: 
- Distribution : represent the global statistical distribution of a set of values calculated accross the infrastructure.
- Counts: this host emit the following values in a flush interval [1,1,1,2,2,2,3,3], agent will submit data point with 15 as a value.
- rates : this host emit the following values in a flush interval [1,1,1,2,2,2,3,3], agent will submit data point with 1.5 as a value.
- gauges: this host emit the following values in a flush interval [71,71,71,71,71,71,71.5], agent will submit data point with 71.5 as a value.  

# APM tracer
you have developed an application and you want datdog to display data from your application, then 2 step envolved: 
* datadog agent installed 
* APM tracer installed 
--- what's APM tracer ---- 
APM tracer gives a detailed snapshot of a "single" transaction in your application, a transaction trace record: 
* the availbale function call 
* database calls 
* external call 
as an example .NET Tracer is the software which is used to collect the trace.  

# SLA, SLO, SLI and Error Budget 
SRE engineer: Site Reliability Engineering: must work on making SYSTEM RELIABLE (RELIABLE = keeping system stable)
*** SRE Task and Responsibilities *** 
1 automation: create automated process for operational aspects. 
2 Configure MOnitoring and Logging: Observability for system perfermance
3 Alerting 
4 Develop custom service to achieve this
5 Postmortem (after death but in latin): analysis after issue 
######################################################################################
The goals of 3 things (SLA, SLO, SLI) is to have an "Agreement" between us (provider) and the client. So both parts can be on the same page regarding application perfermance.  

*** SLA: Service Level Agreements: this is the agreement (contract) which you make with your users or clients. 
*** SLO: Service Level Objectives: those are the objectives (promises, goals) which your team must hit to meet that agreement (SLA). 
*** SLI: Service Level Indicator: The real numbers of the perfermance who  determines is SLO successful.
# SLA : 
AS AN EXAMPLE: 
- success rate need to be 99.50% 
- System should be available 24 houres per day 
=> SLA are difficult to mesure, report on. 
# SLO : 
AS AN EXAMPLE: 
if the SLA is the success rate need to be 99.50%, SO the SLO defines the promise: 
    * what kind of metrics we will use to measure this. 
    * what is the customer expectation. 
    * what IT/DEVOPS should hit to cover this promise 
# SLI : 
SLI is the actual measured value using the SLO metrics and the real value for example 91% 
# Error Budget 
for example we define SLO for success rate to be 99.9%. 
this means that our error Budget is 100% - 99.9% = 0.1%. 
if our service receives 1000000 requests in a month , the "Error Budget" is 0.1% * 1000000 = 1000 request and its good . 

60% from the error Budget is 600 requests

############################################################################################################################################################################ 
# LOG MANAGEMENT 
# 1 Log Entry (like promtail) 
log entry is the simplest data structure, this is a data which structure you define and you want to store somewhere. 
it is like time-stamped documentation of event relevant to a particular system. (see tof Log Entry)
Log Entry contain: 
    * time stamp 
    * type of log (error, warning, info, debug)
    * api information ... 
# 2 LMS (like elasticsearch)
LMS (log management system) is a software solution that involves gathering, processing, storing and analyzing log data from our applications. 
LMS helps us: 
* pinpinting areas of poor perfermance. 
* assessing application health and troubleshooting 
* diagnosing and identifying the root cause of application installation and run-time errors . 
# How to save logs in datadog ? 
we use a "logger object" which is used to log messages for a specific system or application component. 
For different languages Datadog offers different logger solution:  
- Java: LOG4J, LogBACK, 
- nodejs: (express/nestjs): Winston 
- .NET: NLOG 

