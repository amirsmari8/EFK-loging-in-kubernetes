# EFK Loging In Kubernetes

Set up elastic stack in kubernetes cluster (EFK) Elastic - FluentD - Kibana 
Demo with java-app and nodejs-app
# Elastisearch and kibana 
http://localhost:9200/ 
http://localhost:5601/


With following steps:

    configure Java and NodeJS applications to produce logs, package them into Docker images and push into a Docker private or public  repository.

    create Kubernetes cluster on a cloud platform (Linode Kubernetes Engine)

    deploy these application Docker images in the cluster

    deploy ElasticSearch, Kibana and Fluentd in the cluster

    configure Fluentd to start collecting and processing the logs and sending them to ElasticSearch

    configure Kibana to visualise the log data stored in ElasticSearch 
--- 
# Elastic search: 
Elasticsearch is Document oriented : 
* Insert Documents (JSON documents)
* Delete Documents 
* Retrieve Documents 
* Analyse Documents 
* Search Documents the most important. 

=> Documents in Elastic Search are Immutable; if we change a value of a field , the document will be updated from version 1 to version 2 .  

Note: Index support only 1 type.
